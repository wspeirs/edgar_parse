import multiprocessing as mp
import sys

from xbrl.xbrl import XBRL
from reports.balance_sheet import BalanceSheet
from reports.income import IncomeStatement
from reports.cash_flow import CashFlow


def parse_file(file):
    try:
        xbrl = XBRL(file)  # get the XBRL instance
    except Exception as e:
        print("PARSE_ERROR: %s: %s" % (file, e))
        sys.stdout.flush()
        return

    try:
        if not BalanceSheet(xbrl).get_statement():
            # print("BS NOT FOUND FOR: %s" % file)
            pass

        if not IncomeStatement(xbrl).get_statement():
            # print("IS NOT FOUND FOR: %s" % file)
            pass

        if not CashFlow(xbrl).get_statement():
            # print("CF NOT FOUND FOR: %s" % file)
            pass

        sys.stdout.flush()

        # IncomeStatement(xbrl)
    except Exception as e:
        print("REPORT_ERROR: %s: %s" % (file, e))
        sys.stdout.flush()


if __name__ == '__main__':
    pool = mp.Pool(processes=50)

    with open('/tmp/sorted', 'r') as f:
        files = ['/export/stock_files' + l.strip()[1:] for l in f.readlines()]
        # files = [l.strip() for l in f.readlines()]

        files = filter(lambda f: '200' not in f, files)

        pool.map(parse_file, files)

        # for file in files:
        #     parse_file(file)

        # parse_file('/export/stock_files/a/20101031/a-20101031.xml')
        # parse_file('/export/stock_files/imkta/20120929/imkta-20120929.xml')
