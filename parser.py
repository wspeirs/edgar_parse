

xbrl_parser = XBRLParser()
xbrl = xbrl_parser.parse(open("/tmp/fds-20161130.xml"))

gaap_obj = xbrl_parser.parseGAAP(xbrl, doc_date="20161130")

serializer = GAAPSerializer()
result = serializer.dump(gaap_obj)

print(result)
