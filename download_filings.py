import requests

from os import makedirs
from requests_futures.sessions import FuturesSession
from lxml import etree
from bs4 import BeautifulSoup

RSS_URL = "https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK={}&type=10&owner=exclude&count=100&output=atom"
BASE_URL = "https://www.sec.gov"
BASE_DIR = "/export/stock_files/"

session = requests.Session()
future_session = FuturesSession(max_workers=6)  # make 6 workers, one for each URL we'll fetch

with open('sic.txt', 'r') as f:
    lines = f.readlines()

    ciks = [l.split("\t")[1] for l in lines[1:]]

total = len(ciks)
count = 0

for cik in ciks:
    count += 1
    print('Fetching {}\t{} of {}'.format(cik, count, total))

    try:
        parser = etree.XMLParser(ns_clean=True, recover=True)

        ret = session.get(RSS_URL.format(cik))

        if ret.status_code != 200:
            print("Error getting RSS for {}: {}".format(cik, ret.status_code))
            continue

        root = etree.fromstring(ret.content, parser)

        # with open('/tmp/test_none.txt', 'r') as f:
        #     tree = etree.parse(f, parser)
        #     root = tree.getroot()

        ns = root.nsmap[None]

        for entry in root.findall('{%s}entry'%ns):
            type = entry.find('{%s}category'%ns).attrib.get('term')

            # only want 10-K and 10-Q reports
            if type not in ['10-Q', '10-K']:
                continue

            content = entry.find('{%s}content'%ns)

            if content is None:
                continue

            # only want ones with XBRL
            if content.find('{%s}xbrl_href'%ns) is None:
                continue

            # get the filing page URL
            filing_href = content.find('{%s}filing-href'%ns)

            if filing_href is None:
                continue

            print("\tFetching {}".format(filing_href.text))

            try:
                # request the filing page
                ret = session.get(filing_href.text)

                if ret.status_code != 200:
                    print("Error getting {}: {}".format(filing_href.text, ret.status_code))
                    continue

                # parse the page
                soup = BeautifulSoup(ret.text, 'lxml')

                # get all the URLs the end in .xml or .xsd
                xbrl_urls = list(map(lambda a: a['href'], filter(lambda a: a['href'].endswith('.xml') or a['href'].endswith('.xsd'), soup.find_all('a'))))

                requests = []

                # make all the requests for the pages
                for url in xbrl_urls:
                    requests.append((url, future_session.get(BASE_URL + url)))

                s = requests[0][0].rfind('/')+1
                e = requests[0][0].rfind('-')

                ticker = requests[0][0][s:e]

                e2 = requests[0][0].rfind('.')

                date = requests[0][0][e+1:e2]

                path = BASE_DIR + ticker + '/' + date

                # create the directory
                makedirs(path)

                for req in requests:
                    res = req[1].result()

                    if res.status_code != 200:
                        print("Error fetching %s: %d" % (req[0], res.status_code))
                        continue

                    file_name = req[0][req[0].rfind('/')+1:]

                    with open(path + '/' + file_name, 'w') as f:
                        f.write(res.text)
            except Exception as e:
                print("\tError: {}".format(e))
                continue

    except Exception as e:
        print("Error processing {}: {}".format(cik, e))
