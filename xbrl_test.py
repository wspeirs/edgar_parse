from xbrl.xbrl import XBRL

from xbrl.context import Period

from reports.disclosures import Disclosures
from reports.balance_sheet import BalanceSheet
from reports.income import IncomeStatement
from reports.cash_flow import CashFlow

# xbrl_instance = XBRL('samples/fds-10K-20170831/fds-20170831.xml')
# xbrl_instance = XBRL('samples/dyn-10Q-20100630/dyn-20100630.xml')
# xbrl_instance = XBRL('samples/v-10Q-20170630/v-20170630.xml')
xbrl_instance = XBRL('samples/cbk-10Q-20171028/cbk-20171028.xml')

# xbrl_instance = XBRL('/export/stock_files/a/20091031/a-20091031.xml')
# xbrl_instance = XBRL('/export/stock_files/a/20100430/a-20100430.xml')
# xbrl_instance = XBRL('/export/stock_files/hill/20120331/hill-20120331.xml')
# xbrl_instance = XBRL('/export/stock_files/koss/20160331/koss-20160331.xml')
# xbrl_instance = XBRL('/export/stock_files/iweb/20121231/iweb-20121231.xml')
# xbrl_instance = XBRL('/export/stock_files/qpsa/20111231/qpsa-20111231.xml')
# xbrl_instance = XBRL('/export/stock_files/idxg/20151231/idxg-20151231.xml')

# print out the raw facts
# for fact in xbrl_instance.all_facts():
#     print(str(fact)[:300])
# exit()

dises = Disclosures(xbrl_instance)
BalanceSheet(xbrl_instance).get_stmt()
IncomeStatement(xbrl_instance)
CashFlow(xbrl_instance)

# for dis in dises.get_text_iterator():
#     print(dis)
exit()


def recurse(arcs, cur_loc, indent):
    facts = xbrl_instance.get_facts(cur_loc.href)

    if len(facts) != 0:
        for fact in facts:
            print(indent + str(fact))
    else:
        labels = xbrl_instance.get_labels(cur_loc)
        label = max(map(lambda l: str(l), labels), key=len)
        print(indent + label)

    for arc in filter(lambda a: a.from_loc == cur_loc, arcs):
        recurse(arcs, arc.to_loc, indent + ' ')


pres_graph = xbrl_instance.get_presentation_graph()

# for link in xbrl_instance.get_statement_links():
# for link in xbrl_instance.get_document_links():
for link in xbrl_instance.get_disclosure_links():
    print("ID: %s\t%s" % (link.role_type.id, link.role_type.title))

    froms = set([l.from_loc for l in link.arcs])
    tos = set([l.to_loc for l in link.arcs])

    # find the root of the report tree
    root = list(froms - tos)[0]

    recurse(link.arcs, root, '')

    print()


exit(1)



def print_graph(from_loc, indent):
    print(indent + str(from_loc.href))

    # if from_loc.href in xbrl_instance.get_gaap_facts():
    #     print(indent + str(xbrl_instance.get_gaap_facts()[from_loc.href]))
    # else:
    #     print(indent + str(list(xbrl_instance.get_labels(from_loc.href))))

    for to_loc in xbrl_instance.get_presentation_graph().links[from_loc]:
        print_graph(to_loc, indent + ' ')



# for loc in filter(lambda l: 'us-gaap' in l.href, pres_graph.root_report_elements):
for loc in pres_graph.root_report_elements:
    print("!!!")
    print_graph(loc, '')
