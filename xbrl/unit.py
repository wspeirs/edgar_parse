
class Unit:
    def __init__(self, id, measure=None, numerator=None, denominator=None):
        self.id = id
        self.measure = measure
        self.numerator = numerator
        self.denominator = denominator

        if (self.numerator is not None and self.denominator is None) or \
           (self.numerator is None and self.denominator is not None):
            raise ValueError("Both numberator and denominator must be set or not set")

    def __str__(self):
        if self.measure is not None:
            return "$" if 'USD' in self.measure else self.measure
        else:
            return "%s/%s" % (self.numerator, self.denominator)

    def __eq__(self, other):
        return other is not None and self.__dict__ == other.__dict__

    @staticmethod
    def from_xml(element, namespace=None):
        if namespace is None:
            namespace = element.nsmap['xbrli']

        measure = element.find("{%s}measure" % namespace)
        numerator = element.find(".//{%s}numerator" % namespace)
        denominator = element.find(".//{%s}denominator" % namespace)

        measure = measure.text if measure is not None else None
        numerator = numerator.find("{%s}measure" % namespace).text if numerator is not None else None
        denominator = denominator.find("{%s}measure" % namespace).text if denominator is not None else None

        return Unit(element.attrib['id'], measure, numerator, denominator)


# constants
US_DOLLARS = Unit('u000', 'iso4217:USD')

