import requests

from os.path import isdir, isfile, join, basename, realpath, dirname
from os import listdir
from lxml import etree
from urllib.parse import urlparse


# Note: us-gaap taxonomies can be fetched from:
# http://xbrl.fasb.org/us-gaap/2017/elts/us-gaap-2017-01-31.xsd
# http://xbrl.sec.gov/currency/2017/currency-2017-01-31.xsd

class TaxonomyElement:
    def __init__(self, id, name, nillable, sub_group, type, period, abstract=False):
        self.id = id
        self.name = name
        self.nillable = nillable
        self.sub_group = sub_group
        self.type = type
        self.period = period
        self.abstract = abstract

    @staticmethod
    def from_xml(element):
        try:
            return TaxonomyElement(
                element.attrib['id'],
                element.attrib['name'],
                element.attrib.get('nillable', False),
                element.attrib.get('substitutionGroup', None),
                element.attrib['type'],
                element.attrib.get('periodType', None),
                element.attrib.get('abstract', None)
            )
        except KeyError:
            return None


class Taxonomy:
    def __init__(self, taxonomy_file):
        if not isfile(taxonomy_file):
            raise ValueError("Cannot find file: " + taxonomy_file)

        #
        # TODO: Need to be able to handle XML files as well as XSD files
        #

        self.taxonomy_file = taxonomy_file
        self.target_namespace = None
        self.id_element_dict = None

    def __parse_file(self):
        parser = etree.XMLParser(ns_clean=True)
        tree = etree.parse(self.taxonomy_file, parser)
        root = tree.getroot()

        if 'targetNamespace' in root.attrib:
            self.target_namespace = root.attrib['targetNamespace']

        xs_ns = root.nsmap[root.prefix]

        self.id_element_dict = dict()

        for ele in filter(lambda e: e.tag == "{%s}element" % xs_ns, root):
            te = TaxonomyElement.from_xml(ele)

            if te is None:  # just punt here
                continue

            self.id_element_dict[te.id] = te

    def get_element(self, id):
        # lazy creation
        if self.id_element_dict is None:
            self.__parse_file()

        return self.id_element_dict.get(id)


class TaxonomyManager:
    def __init__(self, directory=join(dirname(__file__), '../taxonomies')):
        """
        Initializes the taxonomy manager
        :param files: a list of files to explicitly include
        :param directory: a directory to search for taxonomies
        """
        if not isdir(directory):
            raise ValueError("Invalid directory: " + directory)

        self.directory = directory

        # this stores taxonomy name -> taxonomy
        self.taxonomies = {f: Taxonomy(join(directory, f)) for f in listdir(directory) if isfile(join(directory, f))}

        # parse the taxonomies document for where to find new taxonomies
        parser = etree.XMLParser(ns_clean=True)

        self.tree = etree.parse(join(self.directory, 'edgartaxonomies.xml'), parser)

        root = self.tree.getroot()

        self.taxonomy_locations = dict()

        for loc in root:
            # only want US GAAP and the IM version (not 100% sure why)
            if loc.find('Family').text != 'US GAAP' or 'IM' not in loc.find('Version').text:
                continue

            if loc.find('Namespace') is None:
                continue

            path = urlparse(loc.find('Namespace').text).path.split('/')

            # not sure how to deal with these
            if len(path) != 3:
                continue

            self.taxonomy_locations["%s-%s.xsd"%(path[1], path[2])] = loc.find('Href').text

        # setup a requests session
        self.session = requests.Session()

    def add_files(self, files):
        """
        Add a set of files to the taxonomy manager.
        :param files: the files to add
        """
        if not isinstance(files, list):
            files = [files]

        for f in files:
            if not isfile(f):
                raise ValueError("Invalid file: " + f)

            self.taxonomies[basename(f)] = Taxonomy(f)

    def get_taxonomy(self, taxonomy, file=None):
        # try to see if we can find it in the same directory
        if taxonomy not in self.taxonomies:
            # if not, then try to pull from the web and save
            if taxonomy not in self.taxonomy_locations:
                return None

            # try to fetch it from the web
            res = self.session.get(self.taxonomy_locations[taxonomy])

            if res.status_code != 200:
                raise ValueError("Error fetching %s: %d" %(self.taxonomy_locations[taxonomy], res.status_code))

            with open(join(self.directory, taxonomy), 'w') as f:
                f.write(res.text)

            self.taxonomies[taxonomy] = None  # this will get parsed below

        # perform the lazy eval
        if self.taxonomies[taxonomy] is None:
            self.taxonomies[taxonomy] = Taxonomy(join(self.directory, taxonomy))

        return self.taxonomies[taxonomy]
