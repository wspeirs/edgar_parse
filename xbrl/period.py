from dateutil import parser


class Period:
    def __init__(self, instant=None, start_date=None, end_date=None):
        self.instant = parser.parse(instant).date() if instant is not None else None
        self.startDate = parser.parse(start_date).date() if start_date is not None else None
        self.endDate = parser.parse(end_date).date() if end_date is not None else None

        if (self.startDate is not None and self.endDate is None) or \
           (self.startDate is None and self.endDate is not None):
            raise ValueError("Both start_date and end_date must be set or not set")

    def get_type(self):
        if self.instant is not None:
            return "instant"
        elif self.startDate is not None and self.endDate is not None:
            return "duration"
        else:
            return "forever"

    def get_days(self):
        """
        Gets the number of days in this period
        :return: endDate - startDate or 1 for instant
        """
        if self.get_type() == 'instant':
            return 1
        elif self.get_type() == 'duration':
            return (self.endDate-self.startDate).days
        else:
            return 0

    def get_end_date(self):
        """
        Returns either instant or the endDate, depending upon type
        :return: date object representing the end date
        """
        if self.get_type() == 'instant':
            return self.instant
        elif self.get_type() == 'duration':
            return self.endDate
        else:
            raise TypeError("No end date to forever periods")

    def __str__(self):
        if self.get_type() == 'instant':
            return str(self.instant)
        elif self.get_type() == 'duration':
            return "%s -> %s" % (self.startDate, self.endDate)
        else:
            return "forever"

    def __hash__(self):
        return hash(self.instant.__hash__() + self.startDate.__hash__() + self.endDate.__hash__())

    def __eq__(self, other):
        return self.instant == other.instant and self.startDate == other.startDate and self.endDate == other.endDate

    def __ne__(self, other):
        return not self.__eq__(other)

    def __lt__(self, other):
        if self.instant is not None and other.instant is not None:
            return self.instant < other.instant
        elif self.startDate is not None and other.startDate is not None:
            return self.startDate < other.startDate
        else:
            raise NotImplemented("lt not implemented for %s and %s" % (self.get_type(), other.get_type()))

    def __le__(self, other):
        return self.__lt__(other) or self.__eq__(other)

    def __gt__(self, other):
        return not self.__le__(other)

    def __ge__(self, other):
        return not self.__lt__(other) or self.__eq__(other)


    @staticmethod
    def from_xml(element, namespace=None):
        if namespace is None:
            namespace = element.nsmap['xbrli']

        instant = element.find("{%s}instant" % namespace)
        start_date = element.find("{%s}startDate" % namespace)
        end_date = element.find("{%s}endDate" % namespace)

        instant = instant.text if instant is not None else None
        start_date = start_date.text if start_date is not None else None
        end_date = end_date.text if end_date is not None else None

        return Period(instant, start_date, end_date)

