from lxml import etree
from collections import defaultdict
from xbrl.location import Location


class Label:
    def __init__(self, label, role, language, text):
        self.label = label
        self.role = role
        self.language = language
        self.text = text

    def __str__(self):
        return self.text if self.text is not None else ''

    @staticmethod
    def from_xml(element, xlink_ns, xlm_ns):
        label = element.attrib["{%s}label" % xlink_ns]
        role = element.attrib["{%s}role" % xlink_ns]
        language = element.attrib["{%s}lang" % xlm_ns]
        text = element.text

        return Label(label, role, language, text)


class LabelGraph:
    def __init__(self, taxonomy_manager, label_file):
        self.label_file = label_file

        parser = etree.XMLParser(ns_clean=True)

        self.tree = etree.parse(label_file, parser)

        root = self.tree.getroot()

        if 'xlink' not in root.nsmap:
            raise ValueError("Cannot find xlink in namespace map of the root for %s" % label_file)

        xml_ns = 'http://www.w3.org/XML/1998/namespace'  # not found in the namespace of the root :-\

        prefix_ns = root.nsmap[root.prefix]
        xlink_ns = root.nsmap['xlink']

        label_link_element = root.find("{%s}labelLink"%prefix_ns)

        if label_link_element is None:
            raise ValueError("Cannot find the labelLink element")

        # init some vars
        labels = []
        locations = []

        # collect all the labels and locations
        for ele in filter(lambda e: e.tag == "{%s}label"%prefix_ns or "{%s}loc"%prefix_ns, label_link_element):
            if ele.tag == "{%s}label" % prefix_ns:
                labels.append(Label.from_xml(ele, xlink_ns, xml_ns))
            elif ele.tag == "{%s}loc"%prefix_ns:
                locations.append(Location.from_xml(ele, xlink_ns, taxonomy_manager))

        # build the label lookup dictionary
        label_lookup = defaultdict(lambda: [])

        for label in labels:
            label_lookup[label.label].append(label)

        # create the location lookup dictionary
        self.location_lookup = {x.label: x for x in locations}

        # create the location to label dictionary
        self.location_to_labels = dict()

        for ele in filter(lambda e: e.tag == "{%s}labelArc"%prefix_ns, label_link_element):
            # arc_role = ele.attrib["{%s}arcrole"%xlink_ns] punting on this for now
            _from = self.location_lookup[ele.attrib["{%s}from"%xlink_ns]]
            _to = label_lookup[ele.attrib["{%s}to"%xlink_ns]]

            self.location_to_labels[_from.href] = _to

    def get_labels(self, location):
        """
        Given a location, get the labels for it.

        :param location: the location
        :return: a list of labels for the location
        """
        if isinstance(location, Location):
            return self.location_to_labels.get(location.href, [])
        else:
            return self.location_to_labels.get(location, [])

