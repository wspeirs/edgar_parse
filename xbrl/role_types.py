import re

class RoleType(object):
    def __init__(self, id, uri, definition):
        self.id = id
        self.uri = uri

        definition = definition.replace('\n', ' ')  # clean-up any newlines
        fields = definition.split(' - ', 2)

        if len(fields) == 0:
            raise ValueError("Empty definition: %s" % definition)

        if len(fields) != 3:  # try to mend-up broken/invalid files
            if re.match('[0-9]+', fields[0]):
                sort_code = fields[0]
            else:
                sort_code = '0'

            if fields[1].lower() in ('statement', 'disclosure', 'document'):
                type = fields[1]
                title = ''
            elif 'statement' in fields[1].lower() or 'sheet' in fields[1].lower():
                type = 'Statement'
                title = fields[1]
            else:
                type = 'Document'
                title = fields[1]
        else:  # this is the case 99% of the time
            (sort_code, type, title) = fields

            if sort_code is None or type is None or title is None:
                raise ValueError("Bad definition: %s" % definition)

        self.sort_code = sort_code
        self.type = type
        self.title = title

    def __str__(self):
        return "%s: %s" % (self.type, self.title)

    @staticmethod
    def from_xml(element, link_ns):
        id = element.attrib.get('id')
        uri = element.attrib.get('roleURI')
        definition = element.find("{%s}definition" % link_ns).text

        return RoleType(id, uri, definition)
