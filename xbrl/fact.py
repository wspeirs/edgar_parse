from xbrl.context import Context
from xbrl.unit import Unit


# TODO: add precision/decimals

class Fact:
    def __init__(self, name, context, unit, value, labels):
        if not isinstance(context, Context):
            raise TypeError("context parameter is not a Context")

        if unit is not None and not isinstance(unit, Unit):
            raise TypeError("unit parameter is not a Unit")

        if not isinstance(labels, list):
            raise TypeError("labels parameter is not a list")

        self.name = str(name)
        self.context = context
        self.unit = unit
        try:
            self.value = float(value)
        except (ValueError, TypeError):
            self.value = str(value)
        self.labels = labels

    def __str__(self):
        label = self.get_label_str()

        if isinstance(self.value, float):
            value = "{:,.2f}".format(self.value)
        else:
            value = self.value

        if self.unit is None or self.unit.measure == 'xbrli:shares':
            unit_str = ''
        else:
            unit_str = str(self.unit)

        return "%s: %s%s (%s)" % (label, unit_str, value, self.context)

    def __hash__(self):
        return hash(self.name.__hash__() +
                    self.context.__hash__() +
                    self.unit.__hash__() +
                    self.value.__hash__() +
                    sum(map(lambda l: l.__hash__(), self.labels)))

    def __eq__(self, other):
        return self.name == other.name and \
               self.context == other.context and \
               self.unit == other.unit and \
               self.value == other.value and \
               self.labels == other.labels

    def get_label_str(self, combined=False):
        """
        Returns a string representing the label of this fact. If combined is True, then joins all together
        :return: a string for this fact
        """
        if combined:
            return ', '.join(map(lambda l: str(l), self.labels))
        else:
            return max(map(lambda l: str(l), self.labels), key=len)  # just go with the longest
