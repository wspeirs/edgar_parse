

class Location:
    def __init__(self, name, label, type):
        self.href = name
        self.label = label
        self.type = type

    def __str__(self):
        return self.label

    def __hash__(self):
        return hash(self.href + self.label + self.type)

    def __eq__(self, other):
        return self.href == other.href and \
               self.label == other.label and \
               self.type == other.type

    @staticmethod
    def from_xml(element, xlink_ns, taxonomies):
        href = element.attrib["{%s}href" % xlink_ns]

        if href is None or len(href) == 0:
            return None

        hash_index = href.rfind('#')
        id = href[hash_index+1:]
        taxonomy_name = href[href.rfind('/')+1:hash_index]

        taxonomy = taxonomies.get_taxonomy(taxonomy_name)

        if taxonomy is None:
            raise ValueError("Cannot find taxonomy: " + taxonomy_name)

        name = taxonomy.get_element(id)

        if name is None:
            raise ValueError("Could not find element by ID: %s" % id)

        name = name.name

        label = element.attrib["{%s}label" % xlink_ns]
        type = element.attrib["{%s}type" % xlink_ns]

        return Location("{%s}%s" % (taxonomy.target_namespace, name), label, type)

