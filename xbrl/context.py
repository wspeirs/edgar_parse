from xbrl.period import Period

class Entity:
    def __init__(self, identifier, scheme):
        if identifier is None:
            raise ValueError("Identifier must be set")

        if scheme is None:
            raise ValueError("Scheme must be set")

        self.indentifier = str(identifier)
        self.scheme = str(scheme)

    def __str__(self):
        return "%s (%s)" % (self.indentifier, self.scheme)


class ExplicitMember:
    def __init__(self, dimension, value):
        self.dimension, self.value = str(dimension), str(value)


class Context:
    def __init__(self, id, entity, period, explicit_members):
        """
        Constructs a Context from an id, entity, and period
        :param id: the context's id
        :param entity: the entity
        :param period: the period
        """
        if not isinstance(entity, Entity):
            raise TypeError("Entity must be of type Entity")

        if not isinstance(period, Period):
            raise TypeError("Period must be of type Period")

        if not isinstance(explicit_members, list):
            raise TypeError("Explicit Members must be of type List")

        self.id = id
        self.entity = entity
        self.period = period
        self.explicit_members = explicit_members

    def __str__(self):
        return str(self.period)
