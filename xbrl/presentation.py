from lxml import etree
from xbrl.location import Location
from xbrl.role_types import RoleType

from collections import defaultdict


class PresentationArc:
    def __init__(self, from_loc, to_loc, order, preferred_label, title, closed=True):
        self.from_loc = from_loc
        self.to_loc = to_loc
        self.order = int(float(order))
        self.preferred_label = preferred_label
        self.title = title
        self.closed = closed

    def __str__(self):
        return '"%s" -> "%s";' % (self.from_loc, self.to_loc)
        # return self.title if self.title is not None else self.preferred_label

    def __hash__(self):
        return hash(self.from_loc.__hash__() +
                    self.to_loc.__hash__() +
                    self.order.__hash__() +
                    self.preferred_label.__hash__() +
                    self.title.__hash__() +
                    self.closed.__hash__())

    def __eq__(self, other):
        return self.from_loc == other.from_loc and \
               self.to_loc == other.to_loc and \
               self.order == other.order and \
               self.preferred_label == other.preferred_label and \
               self.title == other.title and \
               self.closed == other.closed


class PresentationLink:
    def __init__(self, role_type, locations, arcs):
        # if not isinstance(locations, list):
        #     raise TypeError("Locations must be a list")

        if not isinstance(arcs, list):
            raise TypeError("Arcs must be a list")

        self.role_type = role_type
        # self.locations = set(locations)
        self.arcs = sorted(arcs, key=lambda a: a.order)

    def __str__(self):
        return self.role_type.title

    @staticmethod
    def from_xml(element, link_ns, xlink_ns, xbrldt_ns, role_types, taxonomies):
        locations = []
        arcs = []

        for loc in element.findall("{%s}loc"%link_ns):
            location = Location.from_xml(loc, xlink_ns, taxonomies)
            if location is not None:
                locations.append(location)

        location_lookup = {x.label: x for x in locations}

        # get all of the arcs
        for arc in element.findall("{%s}presentationArc"%link_ns):
            from_loc = location_lookup[arc.attrib['{%s}from'%xlink_ns]]
            to_loc = location_lookup[arc.attrib['{%s}to'%xlink_ns]]

            order = arc.attrib['order']
            pref_label = arc.attrib.get('preferredLabel', "http://www.xbrl.org/2003/role/label")
            title = arc.attrib.get('{%s}title'%xlink_ns, None)
            closed = True if xbrldt_ns is None else arc.attrib.get('{%s}closed'%xbrldt_ns, True)

            arcs.append(PresentationArc(from_loc, to_loc, order, pref_label, title, closed))

        # get the role of this presentation link
        role = element.attrib["{%s}role"%xlink_ns]

        if role not in role_types:
            # try and create one
            if '/us-gaap/role/' in role:
                parts = role.split('/')
                definition = '00000 - %s - %s' % (parts[-2], parts[-1])
                role_type = RoleType(parts[-1], role, definition)
            else:
                return None
                # raise ValueError("Unknown role type: %s" % role)
        else:
            role_type = role_types[role]

        return PresentationLink(role_type, None, arcs)


class PresentationGraph:
    def __init__(self, taxonomy_manager, presentation_file, role_types):
        self.presentation_file = presentation_file

        parser = etree.XMLParser(ns_clean=True)

        self.tree = etree.parse(presentation_file, parser)

        root = self.tree.getroot()

        if 'xlink' not in root.nsmap:
            raise ValueError("Cannot find xlink in namespace map of the root")

        # if 'xbrldt' not in root.nsmap:
        #     raise ValueError("Cannot find xbrldt in namespace map of the root")

        prefix_ns = root.nsmap[root.prefix]
        xlink_ns = root.nsmap['xlink']
        xbrldt_ns = root.nsmap.get('xbrldt', None)

        # init some vars
        self.roles = []
        self.presentation_links = []

        # for ele in filter(lambda e: e.tag == "{%s}roleRef"%prefix_ns, root):
        #     self.roles.append(Role.from_xml(ele, xlink_ns))

        for ele in filter(lambda e: e.tag == "{%s}presentationLink"%prefix_ns, root):
            pres_link = PresentationLink.from_xml(ele, prefix_ns, xlink_ns, xbrldt_ns, role_types, taxonomy_manager)

            if pres_link is not None:
                self.presentation_links.append(pres_link)

        # collect all of the targets so we can build the graph
        targets = set()
        self.links = defaultdict(lambda : [])

        for link in self.presentation_links:
            for arc in link.arcs:
                targets.add(arc.to_loc)
                self.links[arc.from_loc].append(arc.to_loc)

        # collect all of the root report elements
        self.root_report_elements = set()

        for link in self.presentation_links:
            for arc in link.arcs:
                if arc.from_loc not in targets:
                    self.root_report_elements.add(arc.from_loc)
