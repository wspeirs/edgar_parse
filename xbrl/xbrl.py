from os.path import isfile, dirname, join, abspath
from lxml import etree
from collections import defaultdict

from xbrl.taxonomy import TaxonomyManager
from xbrl.context import *
from xbrl.fact import *
from xbrl.label import LabelGraph
from xbrl.presentation import PresentationGraph
from xbrl.role_types import RoleType


class XBRL:
    """
    Class that handles the XBRL 'bag of facts' and the associated label and presentation files
    """
    def __init__(self, xbrl_file):
        """
        Creates the XBRL instance
        :param xbrl_file: the xbrl file to parse
        """
        # check for our file
        self.xbrl_file = xbrl_file

        if not isfile(self.xbrl_file):
            raise ValueError("Could not find the file: %s" % self.xbrl_file)

        # get the directory of the file
        self.xbrl_dir = abspath(dirname(xbrl_file))

        # create our Taxonomy manager
        self.taxonomy_manager = TaxonomyManager()

        # create the parser and tree for the XBRL file
        try:
            parser = etree.XMLParser(ns_clean=True)
            tree = etree.parse(self.xbrl_file, parser)
        except etree.XMLSyntaxError:  # enable huge_tree and recover if this fails
            parser = etree.XMLParser(ns_clean=True, recover=True, huge_tree=True)
            tree = etree.parse(self.xbrl_file, parser)

        self.xbrl_root = tree.getroot()

        self.files = dict()  # holds the supporting files
        self.role_types = dict()  # holds all the roles

        if 'link' in self.xbrl_root.nsmap:
            link_ns = self.xbrl_root.nsmap['link']
        elif 'xbrll' in self.xbrl_root.nsmap:
            link_ns = self.xbrl_root.nsmap['xbrll']
        else:
            link_ns = self.xbrl_root.nsmap[None]

        # get all the schema docs
        for ele in self.xbrl_root.findall('{%s}schemaRef' % link_ns):
            href = ele.attrib.get('{%s}href' % link_ns)

            if href is None:  # if link doesn't work, try xlink
                href = ele.attrib.get('{%s}href' % self.xbrl_root.nsmap['xlink'])

            if href is None:
                raise ValueError("Cannot find href for %s" % xbrl_file)

            if not href.startswith('http'):  # assume it's something we have in our taxonomies folder
                schema_file = join(self.xbrl_dir, href)
                self.taxonomy_manager.add_files(schema_file)  # add the schema file
                self.__parse_schema(schema_file)  # parse the schema file

        # check all of our files
        for file in self.files.values():
            if not isfile(file):
                raise ValueError("Could not find file: %s" % file)

        # get all the labels for the facts
        try:
            self.__label_graph = LabelGraph(self.taxonomy_manager, self.files['label'])
        except Exception as e:
            raise ValueError("Error making label graph for %s: %s" % (self.files['label'], e))

        # parse our the presentation file
        if 'presentation' in self.files:
            self.__presentation_graph = PresentationGraph(self.taxonomy_manager, self.files['presentation'], self.role_types)

        # parse the actual XBRL file
        self.__parse_xbrl()

    def __parse_schema(self, schema_file):
        """
        Given a schema file, parse it for the supporting files (linkbaseRef) and roles
        :param schema_file: the schema file
        """
        parser = etree.XMLParser(ns_clean=False)
        tree = etree.parse(schema_file, parser)

        root = tree.getroot()

        xs_ns = root.nsmap[root.prefix]

        # get the custom prefix
        target_namespace = root.attrib['targetNamespace']

        for prefix, ns in root.nsmap.items():
            if ns == target_namespace:
                self.custom_prefix = prefix
                break

        # get the app info element
        appinfo = root.find("{%s}annotation"%xs_ns).find("{%s}appinfo"%xs_ns)

        # get all of the supporting files
        # this is a bit janky, but some files were missing link in their outter-most nsmap
        for ele in filter(lambda e: 'linkbaseRef' in str(e.tag), appinfo):
            xlink_ns = ele.nsmap['xlink']

            if "{%s}role"%xlink_ns in ele.attrib:
                role = ele.attrib.get("{%s}role"%xlink_ns)
                role = role[role.rfind('/')+1:].replace('LinkbaseRef', '')
            elif "{%s}arcrole"%xlink_ns in ele.attrib:
                role = ele.attrib.get("{%s}href"%xlink_ns)
                role = 'presentation' if '_pre' in role else role
                role = 'label' if '_lab' in role else role
                role = 'definition' if '_def' in role else role
                role = 'calculation' if '_cal' in role else role
            else:
                continue  # just keep moving

            # add the supporting files
            href = ele.attrib.get("{%s}href"%xlink_ns)
            if not href.startswith('http'):  # we assume this is in our taxonomy manager already
                self.files[role] = join(dirname(schema_file), href)

        # get all of the role types
        for ele in filter(lambda e: 'roleType' in str(e.tag), appinfo):
            link_ns = ele.nsmap['link']
            try:
                rt = RoleType.from_xml(ele, link_ns)
            except Exception as e:
                raise ValueError("Error getting roles for %s: %s (%s)" % (self.xbrl_file, str(ele.text), e))
            self.role_types[rt.uri] = rt

    def __parse_xbrl(self):
        if 'xbrli' in self.xbrl_root.nsmap:
            xbrli_ns = self.xbrl_root.nsmap['xbrli']
        else:
            xbrli_ns = self.xbrl_root.nsmap[None]

        # init some vars
        contexts = []
        units = []

        # collect all the context elements
        for ele in filter(lambda e: e.tag == "{%s}context" % xbrli_ns, self.xbrl_root):
            ident_ele = ele.find(".//{%s}identifier" % xbrli_ns)
            period_ele = ele.find("{%s}period" % xbrli_ns)
            segment_ele = ele.find(".//{%s}segment" % xbrli_ns)

            if ident_ele is None or period_ele is None:
                continue

            entity = Entity(ident_ele.text, ident_ele.attrib['scheme'])
            period = Period.from_xml(period_ele, xbrli_ns)

            explicit_members = []

            if segment_ele is not None:
                for em_ele in segment_ele:
                    if 'explicitMember' in ele.tag:
                        explicit_members.append(ExplicitMember(em_ele.attrib['dimension'], em_ele.text))

            contexts.append(Context(ele.attrib['id'], entity, period, explicit_members))

        if len(contexts) == 0:
            raise ValueError("Couldn't find any context in %s" % self.xbrl_file)

        # build the context dictionary
        self.context_lookup = {c.id: c for c in contexts}

        # collect all the unit elements
        for ele in filter(lambda e: e.tag == "{%s}unit" % xbrli_ns, self.xbrl_root):
            units.append(Unit.from_xml(ele, xbrli_ns))

        # build the unit dictionary
        self.unit_lookup = {u.id: u for u in units}

        # we can have multiple facts for a given name/href
        self._facts = defaultdict(lambda: [])

        # collect all the fact elements for us-gaap
        for ele in filter(lambda e: e.prefix in ('us-gaap', 'dei', self.custom_prefix), self.xbrl_root):
            context_ref = ele.attrib['contextRef']
            unit_ref = ele.attrib.get('unitRef', None)

            if context_ref not in self.context_lookup:
                raise ValueError("Context ref not found: %s" % context_ref)
            else:
                context = self.context_lookup[context_ref]

            if unit_ref is None:
                unit = None
            elif unit_ref not in self.unit_lookup:
                raise ValueError("Unit ref not found: %s" % unit_ref)
            else:
                unit = self.unit_lookup[unit_ref]

            labels = self.__label_graph.get_labels(ele.tag)
            self._facts[ele.tag].append(Fact(ele.tag, context, unit, ele.text, labels))

    def get_presentation_graph(self):
        return self.__presentation_graph

    def __get_links(self, type):
        m_type = type.lower()
        return sorted(filter(lambda l: l.role_type.type.lower() == m_type, self.__presentation_graph.presentation_links), key=lambda l: l.role_type.sort_code)

    def get_link_types(self):
        types = set()

        for l in self.__presentation_graph.presentation_links:
            types.add(l.role_type.type)

        return types

    def get_statement_links(self):
        return self.__get_links('Statement')

    def get_disclosure_links(self):
        return self.__get_links('Disclosure')

    def get_document_links(self):
        return list(self.__get_links('Document'))

    def get_facts(self, fact_name, ns=None, latest_first=True):
        """
        Get the list of facts given a name/href
        :param fact_name: the fact's name.
        :param ns: the short-name namespace to use, or None if fact_name is fully qualified
        :param latest_first: if the latest fact should be returned first in the list
        :return: list of Fact objects
        """
        if ns is not None:
            fact_name = '{%s}%s' % (self.xbrl_root.nsmap[ns], fact_name)

        return sorted(self._facts.get(fact_name, []), key=lambda f: f.context.period, reverse=latest_first)

    def get_fact(self, fact_name, period:Period, ns=None):
        """
        Get a fact given its name and period
        :param fact_name: the name of the fact
        :param period: the period
        :param ns: the short-name namespace to use, or None if fact_name is fully qualified
        :return: the fact
        """
        facts = self.get_facts(fact_name, ns)

        if len(facts) == 0:
            return None

        for fact in facts:
            if fact.context.period == period:
                return fact

        return None

    def all_facts(self, period=None):
        """
        Gives a flattened list of all facts
        :return: the list of all facts
        """
        ret = [fact for sublist in self._facts.values() for fact in sublist]

        if period is not None:
            ret = list(filter(lambda f: f.context.period == period, ret))

        return ret

    def get_labels(self, location):
        return self.__label_graph.get_labels(location)

    # def find_facts_by_label(self, fact_label, unit=None, facts=None):
    #     facts = self.all_facts() if facts is None else facts
    #
    #     ret = filter(lambda f: fact_label.lower() in f.get_label_str().lower(), facts)
    #
    #     if unit is not None:
    #         if isinstance(unit, list):
    #             raise TypeError("Unit cannot be a list")
    #         ret = filter(lambda f: f.unit == unit, ret)
    #
    #     return list(ret)

    def get_report_end_date(self):
        """
        Gets the end date (datetime) for the report
        :return: the end date for the report
        """
        ret = set([x.value for x in self.get_facts('DocumentPeriodEndDate', ns="dei")])

        if len(ret) == 0:
            raise ValueError("Couldn't find a report end date")

        if len(ret) != 1:
            raise ValueError("Found multiple report end dates: %s" % ','.join(ret))

        return next(iter(ret))

    def get_report_type(self):
        """
        Gets the type of report which should be 10-Q or 10-K
        :return: the type of the report
        """
        ret = set([x.value.upper() for x in self.get_facts('DocumentType', ns="dei")])

        if len(ret) == 0:
            raise ValueError("Couldn't find a report type")

        if len(ret) != 1:
            raise ValueError("Found multiple report types: %s" % ','.join(ret))

        return next(iter(ret))