from xbrl.xbrl import XBRL
from xbrl.context import Period

from reports.statement import Statement


class IncomeStatement(Statement):
    def __init__(self, xbrl:XBRL, default=None):
        super().__init__(xbrl)
        self.stmt_link = self.__find_statement_link()

        if self.stmt_link is None:
            raise ValueError("Could not find an income statement")

    def __find_statement_link(self):
        # most have one of these IDs
        STMT_IDS = {
            'StatementsOfOperations',
            'StatementsOfOperationsUnaudited',
            'StatementsOfOperationsAndComprehensiveLoss',
            'StatementConsolidatedStatementsOfEarnings',
            'CondensedConsolidatedStatementsOfOperation',
            'ConsolidatedStatementsOfComprehensiveLoss'
        }
        stmt_links = self.xbrl.get_statement_links()

        for link in stmt_links:
            if link.role_type.id in STMT_IDS:
                return link

        # wasn't one of the common ones
        for link in stmt_links:
            if ('operation' in link.role_type.id.lower() or 'income' in link.role_type.id.lower()) \
               and 'paren' not in link.role_type.id.lower():
                return link

        # last ditch, check the title
        for link in stmt_links:
            if ('operation' in link.role_type.title.lower() or 'income' in link.role_type.title.lower()) \
               and 'paren' not in link.role_type.title.lower():
                return link

        return None

    def get_normalized(self, default=None):
        self.fields = {
            # income statement
            'Revenue': ('SalesRevenueNet', 'OilAndGasRevenue'),
            'Cost of Goods Sold': ('CostOfGoodsSold', 'CostOfMerchandiseSalesBuyingAndOccupancyCosts'),
            'Cost of Revenue': ('CostOfRevenue', ),
            'Gross Profit': ('GrossProfit', 'ProfitLoss'),

            # operating expenses
            'Selling, General and Administrative': ('SellingGeneralAndAdministrativeExpense', 'GeneralAndAdministrativeExpense', ),
            'Other Operating Expense': ('OperatingExpenses',),
            'Operating Income': ('OperatingIncomeLoss', 'AdjustmentsToReconcileToIncomeLossFromContinuingOperations'),
            # 'Unusual Expense': ('',),
            'Pretax Income': ('IncomeLossFromContinuingOperationsBeforeIncomeTaxesMinorityInterestAndIncomeLossFromEquityMethodInvestments', ),
            'Net Income': ('NetIncomeLoss', 'ProfitLoss'),
            # 'Interest': (),
            'Income Taxes': ('IncomeTaxExpenseBenefit', ),
            'Depreciation and Amortization': ('CostOfServicesDepreciationAndAmortization', 'DepreciationDepletionAndAmortization'),
        }

        report_type = self.xbrl.get_report_type()

        if report_type == '10-Q':
            context_days = 90
        elif report_type == '10-K':
            context_days = 365
        else:
            raise ValueError("Unknown report type: %s" % report_type)

        # get all the facts
        self._find_facts(self.fields, context_days, default)

        MUST_HAVES = ('Revenue', 'Operating Income', 'Net Income')

        # print the must-haves
        for fact in MUST_HAVES:
            if self.facts[fact] is None:
                print("IS NOT FOUND: %s: %s" % (self.xbrl.xbrl_file, fact))
