from dateutil import parser

from xbrl.xbrl import XBRL
from reports.base_report import BaseReport


class Statement(BaseReport):
    def __init__(self, xbrl: XBRL):
        super().__init__(xbrl)

    def _get_statement(self, stmt_link):
        print("ID: %s\t%s" % (stmt_link.role_type.id, stmt_link.role_type.title))

        roots = BaseReport.get_roots(stmt_link)

        for root in roots:
            self._recurse_loc(stmt_link.arcs, root, lambda i,l: print(' '*i + str(l)), lambda i,f: print(' '*i + str(f)))

        print()

    def _find_facts(self, fields, context_days, default):
        """
        Given a dict of "normalized" fields, find the corresponding facts
        :param fields:
        :param context_days: number of days in the period
        :param default:
        :return:
        """
        self.facts = dict()
        self.end_date = parser.parse(self.xbrl.get_report_end_date()).date()

        # go through and get the fact for each field
        for field, facts in fields.items():
            self.facts[field] = self.__get_fact(facts, context_days, default)

    def __get_fact(self, fact_names, context_days, default):
        if not isinstance(fact_names, tuple):
            raise TypeError("Fact names must be tuple: %s" % str(fact_names))

        # go through the list of fact_names, returning the first one we find
        for fact_name in fact_names:
            for fact in self.xbrl.get_facts(fact_name, ns='us-gaap'):
                if fact.context.explicit_member is not None:
                    continue

                # use +- 10 days to find the proper fact by period
                if fact.context.period.get_end_date() == self.end_date and \
                   abs(context_days - fact.context.period.get_days()) <= 10:
                    return fact

        # by here we haven't found anything
        return default
