from html.parser import HTMLParser
from unicodedata import normalize
from re import sub

from xbrl.xbrl import XBRL
from reports.base_report import BaseReport


class Disclosures(BaseReport):
    class TagStripper(HTMLParser):
        def __init__(self):
            super().__init__()
            self.reset()
            self.text = []

        def handle_data(self, data):
            self.text.append(normalize('NFKD', data))

        def feed(self, data):
            super().feed(data)
            return self

        def get_data(self):
            return ''.join(self.text)

    def __init__(self, xbrl:XBRL):
        super().__init__(xbrl)

        self.disclosures = []

        for disclosure in xbrl.get_disclosure_links():
            roots = BaseReport.get_roots(disclosure)

            for root in roots:
                self._recurse_loc(
                    disclosure.arcs,
                    root,
                    lambda i,l: None,
                    lambda i,f: self.disclosures.append((f.get_label_str(), f.value))
                )

    def __iter__(self):
        self.cur = -1
        return self

    def __next__(self):
        if self.cur >= len(self.disclosures):
            raise StopIteration

        self.cur += 1

        return self.disclosures[self.cur]

    def get_text_iterator(self, strip_html=True):
        """
        Returns an iterator for only those facts that are strings.
        :param strip_html: if True, removes any HTML tags on the text
        :return: an iterator that returns pairs of items: label, fact text
        """
        ret = filter(lambda d: isinstance(d[1], str), self.disclosures)

        if strip_html:
            ret = map(lambda d: (sub(r'\s+\[(\w+\s+)+\w+\]$', '', d[0]), Disclosures.TagStripper().feed(d[1]).get_data()), ret)

        return ret