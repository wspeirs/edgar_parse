from xbrl.xbrl import XBRL
from xbrl.presentation import PresentationLink
from xbrl.location import Location


class BaseReport(object):
    def __init__(self, xbrl: XBRL):
        self.xbrl = xbrl

    def _recurse_loc(self, arcs: list, loc: Location, label_callback: callable, fact_callback: callable):
        """
        Recurse through the links for a given location
        :param arcs: arcs for the given location
        :param loc: current location
        :param label_callback: called for each label that's found with (indent, label)
        :param fact_callback: called for each fact that's found with (indent, fact)
        """
        self.__recurse_loc(arcs, loc, 0, label_callback, fact_callback)

    def __recurse_loc(self, arcs: list, loc: Location, indent: int, label_callback: callable, fact_callback: callable):
        facts = self.xbrl.get_facts(loc.href)

        if len(facts) != 0:  # we have facts for this location
            for fact in filter(lambda f: len(f.context.explicit_members) == 0, facts):
                fact_callback(indent, fact)
        else:
            labels = self.xbrl.get_labels(loc)
            label = max(map(lambda l: str(l), labels), key=len)
            label_callback(indent, label)

        for arc in filter(lambda a: a.from_loc == loc, arcs):
            self.__recurse_loc(arcs, arc.to_loc, indent + 1, label_callback, fact_callback)

    @staticmethod
    def get_roots(link: PresentationLink):
        """
        Gets the roots of a PresentationLink
        :param link: link to find the root of
        :return: the roots of this link
        """
        froms = set([l.from_loc for l in link.arcs])
        tos = set([l.to_loc for l in link.arcs])

        return list(froms - tos)