from xbrl.xbrl import XBRL
from reports.statement import Statement


class BalanceSheet(Statement):
    def __init__(self, xbrl:XBRL):
        super().__init__(xbrl)
        self.stmt_link = self.__find_statement_link()

        if self.stmt_link is None:
            raise ValueError("Could not find a balance sheet")

    def __find_statement_link(self):
        # most have one of these IDs
        STMT_IDS = {
            'CondensedConsolidatedBalanceSheets',
            'CondensedConsolidatedBalanceSheet',
            'CondensedConsolidatedBalanceSheetsUnaudited',
            'CondensedConsolidatedBalanceSheetUnaudited',
            'StatementsOfFinancialConditionUnaudited',
            'Role_StatementOfFinancialPositionUnclassified-SecuritiesBasedOperations'
        }

        stmt_links = self.xbrl.get_statement_links()

        for link in stmt_links:
            if link.role_type.id in STMT_IDS:
                return link

        # wasn't one of the common ones
        for link in stmt_links:
            if 'balancesheet' in link.role_type.id.lower() and 'paren' not in link.role_type.id.lower():
                return link

        # last ditch, check the title
        for link in stmt_links:
            if 'balance' in link.role_type.title.lower() and 'paren' not in link.role_type.title.lower():
                return link

        return None

    def get_stmt(self):
        self._get_statement(self.stmt_link)

    def get_normalized(self, default=None):

        self.fields = {
            # assets
            'Cash and Equivalents': ('CashAndCashEquivalentsAtCarryingValue', 'CashCashEquivalentsAndShortTermInvestments'),
            'Accounts Receivable': ('AccountsReceivableNetCurrent', 'AccountsReceivableNet' ),
            'Inventory': ('InventoryNet', ),
            'Current Assets': ('AssetsCurrent', ),
            'Total Assets': ('Assets', ),

            # liabilities
            'Accounts Payable': ('AccountsPayableCurrent', ),
            'Salaries': ('EmployeeRelatedLiabilitiesCurrent', ),
            'Current Liabilities': ('LiabilitiesCurrent', ),
            'Long Term Debt': ('LongTermDebtNoncurrent', ),
            'Total Liabilities': ('Liabilities', ),

            # equity
            'Retained Earnings': ('RetainedEarningsAccumulatedDeficit', ),
            'Stockholder Equity': ('StockholdersEquity', ),

            # liabilities + equity
            'Total Liability and Equity': ('LiabilitiesAndStockholdersEquity', )
        }

        MUST_HAVES = ('Total Assets', 'Total Liabilities', 'Stockholder Equity', 'Total Liability and Equity')

        self._find_facts(self.fields, 1, default)

        # print the must-haves
        for fact in MUST_HAVES:
            if self.facts[fact] is None:
                print("BS NOT FOUND: %s: %s" % (self.xbrl.xbrl_file, fact))

